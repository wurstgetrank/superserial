﻿namespace SuperSerial
{
    partial class Serial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aSCIIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.autoscrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timestampsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoReconnectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cleardelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.convertSelectionToHexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new SuperSerial.MyRichTextBox();
            this.timerWriteText = new System.Windows.Forms.Timer(this.components);
            this.timerReconnect = new System.Windows.Forms.Timer(this.components);
            this.TSnoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TStimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TSdateTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TSunixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modeToolStripMenuItem,
            this.toolStripSeparator3,
            this.autoscrollToolStripMenuItem,
            this.timestampsToolStripMenuItem,
            this.showToolStripMenuItem,
            this.autoReconnectToolStripMenuItem,
            this.logToolStripMenuItem,
            this.toolStripSeparator1,
            this.cleardelToolStripMenuItem,
            this.toolStripSeparator2,
            this.convertSelectionToHexToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(202, 220);
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aSCIIToolStripMenuItem,
            this.hexToolStripMenuItem});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.modeToolStripMenuItem.Text = "Display Mode";
            // 
            // aSCIIToolStripMenuItem
            // 
            this.aSCIIToolStripMenuItem.Checked = true;
            this.aSCIIToolStripMenuItem.CheckOnClick = true;
            this.aSCIIToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.aSCIIToolStripMenuItem.Name = "aSCIIToolStripMenuItem";
            this.aSCIIToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.aSCIIToolStripMenuItem.Text = "ASCII";
            this.aSCIIToolStripMenuItem.Click += new System.EventHandler(this.aSCIIToolStripMenuItem_Click);
            // 
            // hexToolStripMenuItem
            // 
            this.hexToolStripMenuItem.CheckOnClick = true;
            this.hexToolStripMenuItem.Name = "hexToolStripMenuItem";
            this.hexToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.hexToolStripMenuItem.Text = "Hex";
            this.hexToolStripMenuItem.Click += new System.EventHandler(this.hexToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(198, 6);
            // 
            // autoscrollToolStripMenuItem
            // 
            this.autoscrollToolStripMenuItem.CheckOnClick = true;
            this.autoscrollToolStripMenuItem.Name = "autoscrollToolStripMenuItem";
            this.autoscrollToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.autoscrollToolStripMenuItem.Text = "Autoscroll";
            this.autoscrollToolStripMenuItem.Click += new System.EventHandler(this.autoscrollToolStripMenuItem_Click);
            // 
            // timestampsToolStripMenuItem
            // 
            this.timestampsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSnoneToolStripMenuItem,
            this.TStimeToolStripMenuItem,
            this.TSdateTimeToolStripMenuItem,
            this.TSunixToolStripMenuItem});
            this.timestampsToolStripMenuItem.Name = "timestampsToolStripMenuItem";
            this.timestampsToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.timestampsToolStripMenuItem.Text = "Timestamps";
            this.timestampsToolStripMenuItem.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.timestampsToolStripMenuItem_DropDownItemClicked);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.CheckOnClick = true;
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.showToolStripMenuItem.Text = "Show line endings";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // autoReconnectToolStripMenuItem
            // 
            this.autoReconnectToolStripMenuItem.CheckOnClick = true;
            this.autoReconnectToolStripMenuItem.Name = "autoReconnectToolStripMenuItem";
            this.autoReconnectToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.autoReconnectToolStripMenuItem.Text = "Auto Reconnect";
            this.autoReconnectToolStripMenuItem.Click += new System.EventHandler(this.autoReconnectToolStripMenuItem_Click);
            // 
            // logToolStripMenuItem
            // 
            this.logToolStripMenuItem.Checked = true;
            this.logToolStripMenuItem.CheckOnClick = true;
            this.logToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.logToolStripMenuItem.Name = "logToolStripMenuItem";
            this.logToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.logToolStripMenuItem.Text = "Log";
            this.logToolStripMenuItem.Click += new System.EventHandler(this.logToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(198, 6);
            // 
            // cleardelToolStripMenuItem
            // 
            this.cleardelToolStripMenuItem.Name = "cleardelToolStripMenuItem";
            this.cleardelToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.cleardelToolStripMenuItem.Text = "Clear";
            this.cleardelToolStripMenuItem.Click += new System.EventHandler(this.cleardelToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(198, 6);
            // 
            // convertSelectionToHexToolStripMenuItem
            // 
            this.convertSelectionToHexToolStripMenuItem.Name = "convertSelectionToHexToolStripMenuItem";
            this.convertSelectionToHexToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.convertSelectionToHexToolStripMenuItem.Text = "Convert selection to hex";
            this.convertSelectionToHexToolStripMenuItem.Click += new System.EventHandler(this.convertSelectionToHexToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(31, 509);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(499, 23);
            this.textBox1.TabIndex = 1;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::SuperSerial.Properties.Resources.pausered_fw;
            this.pictureBox1.Location = new System.Drawing.Point(3, 508);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.richTextBox1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(534, 506);
            this.panel1.TabIndex = 3;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.BackColor = System.Drawing.Color.White;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.ContextMenuStrip = this.contextMenuStrip1;
            this.richTextBox1.Location = new System.Drawing.Point(3, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(531, 500);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // timerWriteText
            // 
            this.timerWriteText.Tick += new System.EventHandler(this.timerWriteText_Tick);
            // 
            // timerReconnect
            // 
            this.timerReconnect.Interval = 1000;
            this.timerReconnect.Tick += new System.EventHandler(this.timerReconnect_Tick);
            // 
            // TSnoneToolStripMenuItem
            // 
            this.TSnoneToolStripMenuItem.Name = "TSnoneToolStripMenuItem";
            this.TSnoneToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.TSnoneToolStripMenuItem.Text = "None";
            // 
            // TStimeToolStripMenuItem
            // 
            this.TStimeToolStripMenuItem.Name = "TStimeToolStripMenuItem";
            this.TStimeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.TStimeToolStripMenuItem.Text = "Time";
            // 
            // TSdateTimeToolStripMenuItem
            // 
            this.TSdateTimeToolStripMenuItem.Checked = true;
            this.TSdateTimeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TSdateTimeToolStripMenuItem.Name = "TSdateTimeToolStripMenuItem";
            this.TSdateTimeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.TSdateTimeToolStripMenuItem.Text = "Date + Time";
            // 
            // TSunixToolStripMenuItem
            // 
            this.TSunixToolStripMenuItem.Name = "TSunixToolStripMenuItem";
            this.TSunixToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.TSunixToolStripMenuItem.Text = "Unix";
            // 
            // Serial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(534, 536);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBox1);
            this.KeyPreview = true;
            this.Name = "Serial";
            this.Text = "Serial";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Serial_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Serial_KeyDown);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cleardelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoscrollToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem timestampsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertSelectionToHexToolStripMenuItem;
        private MyRichTextBox richTextBox1;
        private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aSCIIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hexToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Timer timerWriteText;
        private System.Windows.Forms.Timer timerReconnect;
        private System.Windows.Forms.ToolStripMenuItem autoReconnectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TSnoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TStimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TSdateTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TSunixToolStripMenuItem;
    }
}