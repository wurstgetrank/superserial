﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace SuperSerial
{
    [Serializable()]
    public class SerialSettings
    {
        //public SerialPort serialPort;

        public bool connected;
        public string com;
        public Int32 baudrate;
        public Parity parity;
        public Int32 dataBits;
        public StopBits stopBits;
        public string name;

        public Int32 readTimeout;
        public Handshake handshake;
        public bool rtsEnable;

        public SerialSettings(string com, Int32 baudrate, Parity parity, Int32 dataBits, StopBits stopBits, string name)
        {
            connected = false;

            this.com = com;
            this.baudrate = baudrate;
            this.parity = parity;
            this.dataBits = dataBits;
            this.stopBits = stopBits;
            this.name = name;

            readTimeout = 5;
            handshake = Handshake.None;
            rtsEnable = true;
        }
    }
}
