﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using WeifenLuo.WinFormsUI.Docking;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace SuperSerial
{
    public partial class Main : Form
    {
        public List<Serial> serialList;

        VS2015DarkTheme themeDark;
        VS2015LightTheme themeLight;
        VS2015BlueTheme themeBlue;

        bool firsttime = true;

        public Main()
        {
            InitializeComponent();
            serialList = new List<Serial>();

            themeDark = new VS2015DarkTheme();
            themeLight = new VS2015LightTheme();
            themeBlue = new VS2015BlueTheme();
            
            setTheme(Properties.Settings.Default.userTheme);

            this.dockPanel.DocumentStyle = DocumentStyle.DockingWindow;

            LoadPrevious();

            firsttime = false;
        }

        private void addSessionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Create createWindow = new Create(this) { StartPosition = FormStartPosition.CenterScreen};
            createWindow.Show();   
        }

        public DockPanel GetDockPanel()
        {
            return this.dockPanel;
        }

        public void AddSerial(Serial serial)
        {
            serialList.Add(serial);
        }

        public void RemoveSerial(Serial serial)
        {
            serialList.Remove(serial);
        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && e.Modifiers == Keys.Shift)
            {
                foreach(Serial s in serialList)
                {
                    s.ClearWindow();
                }

                e.SuppressKeyPress = true;
            }
        }

        private void darkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setTheme("Dark");
        }

        private void lightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setTheme("Light");
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setTheme("Blue");
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog fontDialog = new FontDialog();
            fontDialog.Font = Properties.Settings.Default.userFont;


            DialogResult result = fontDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.userFont = fontDialog.Font;
                Properties.Settings.Default.Save();
                // TODO save default font

                foreach(Serial s in serialList)
                {
                    s.SetFont(Properties.Settings.Default.userFont);
                }
            }
        }

        private bool themeConfirmation()
        {
            if (dockPanel.Panes.Count > 0)
            {
                DialogResult dialogResult = MessageBox.Show("This will reopen all sessions, continue?", "Change theme", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.No)
                {
                    return false;
                }
            }

            return true;
        }

        private void setTheme(string theme)
        {
            if (themeConfirmation())
            {
                if (!firsttime)
                {
                    SavePrevious();
                    NewProject();
                }

                switch (theme)
                {
                    case "Dark":
                        this.darkToolStripMenuItem.Checked = true;
                        this.lightToolStripMenuItem.Checked = false;
                        this.blueToolStripMenuItem.Checked = false;
                        this.dockPanel.Theme = themeDark;
                        break;
                    case "Light":
                        this.darkToolStripMenuItem.Checked = false;
                        this.lightToolStripMenuItem.Checked = true;
                        this.blueToolStripMenuItem.Checked = false;
                        this.dockPanel.Theme = themeLight;
                        break;
                    case "Blue":
                        this.darkToolStripMenuItem.Checked = false;
                        this.lightToolStripMenuItem.Checked = false;
                        this.blueToolStripMenuItem.Checked = true;
                        this.dockPanel.Theme = themeBlue;
                        break;
                  default:
                        this.darkToolStripMenuItem.Checked = true;
                        this.lightToolStripMenuItem.Checked = false;
                        this.blueToolStripMenuItem.Checked = false;
                        this.dockPanel.Theme = themeDark;
                        break;
                }

                Properties.Settings.Default.userTheme = theme;
                Properties.Settings.Default.Save();

                if (!firsttime)
                {
                    LoadPrevious();
                }
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            SavePrevious();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(serialList.Count > 0)
            {
                DialogResult dialogResult = MessageBox.Show("This will close all open sessions, continue?", "New Project", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.No)
                {
                    return;
                }
                else
                {
                    NewProject();
                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(serialList.Count <= 0)
            {
                return;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "SuperSerial Project|*.bots";
            saveFileDialog.Title = "Save a SuperSerial Project";
            saveFileDialog.OverwritePrompt = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                SaveProject(saveFileDialog.FileName);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(serialList.Count > 0)
            {
                DialogResult dialogResult = MessageBox.Show("This will close all open sessions, continue?", "Load Project", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "SuperSerial Project|*.bots";
            openFileDialog.Title = "Load a SuperSerial Project";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadProject(openFileDialog.FileName);
            }
        }

        private bool SaveProject(string file)
        {
            try
            {
                //this.dockPanel.SaveAsXml(file);

                using (Stream stream = File.Open(file, FileMode.Create))
                {
                    List<SerialSettings> settings = new List<SerialSettings>();

                    foreach (Serial s in serialList)
                    {
                        settings.Add(s.serialSettings);
                    }

                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, settings);

                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Problem saving: " + ex);
                return false;
            }

            return true;
        }

        private void SavePrevious()
        {
            Directory.CreateDirectory("config");
            SaveProject(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "./config/" + "recent.bots");
        }

        private void LoadPrevious()
        {
            // Load recent session
            string recentFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "./config/" + "recent.bots";

            if (File.Exists(recentFile))
            {
                LoadProject(recentFile);
            }
        }

        private bool LoadProject(string file)
        {
            try
            {
                //this.dockPanel.LoadFromXml(file, serialList);

                using (Stream stream = File.Open(file, FileMode.Open))
                {
                    NewProject();

                    BinaryFormatter bin = new BinaryFormatter();
                    List<SerialSettings> settings = new List<SerialSettings>();
                    settings = (List<SerialSettings>)bin.Deserialize(stream);

                    foreach (SerialSettings s in settings)
                    {
                        Create createWindow = new Create(this, s);
                        //createWindow.Show();
                        // TODO load serial window
                    }
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Problem loading: " + ex);
                return false;
            }

            return true;
        }

        private bool NewProject()
        {
            // TODO check if user wants this
            for (int i = serialList.Count - 1; i >= 0; i--)
            {
                serialList[i].Close();
            }

            return true;

        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
