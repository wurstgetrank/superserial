﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using System.IO.Ports;

namespace SuperSerial
{
    public partial class Create : Form
    {
        Main main;

        public Create(Main main)
        {
            this.main = main;

            InitializeComponent();

            // COM Ports
            refreshComPorts();

            comboBoxCom.SelectedIndex = -1;

            // select first available port
            for (int i = 0; i < comboBoxCom.Items.Count; i++)
            {
                string com = comboBoxCom.Items[i].ToString();

                comboBoxCom.SelectedIndex = i;

                if (main.serialList.Count < 1)
                {
                    break;
                }

                foreach (Serial s in main.serialList)
                {
                    if (s.serialSettings.com == com)
                    {
                        if(!s.serialSettings.connected)
                        {
                            comboBoxCom.SelectedIndex = i;
                            break;
                        }
                    }
                }
            }

            // Baudrate
            comboBoxBaudrate.Items.Add("110");
            comboBoxBaudrate.Items.Add("600");
            comboBoxBaudrate.Items.Add("1200");
            comboBoxBaudrate.Items.Add("2400");
            comboBoxBaudrate.Items.Add("4800");
            comboBoxBaudrate.Items.Add("9600");
            comboBoxBaudrate.Items.Add("14400");
            comboBoxBaudrate.Items.Add("19200");
            comboBoxBaudrate.Items.Add("38400");
            comboBoxBaudrate.Items.Add("115200");

            comboBoxBaudrate.SelectedItem = "9600";

            // Parity
            foreach (var item in Enum.GetValues(typeof(Parity)))
            {
                comboBoxParity.Items.Add(item);
            }

            comboBoxParity.SelectedItem = Parity.None;

            // Packetsize
            comboBoxPacketsize.Items.Add("8");
            comboBoxPacketsize.Items.Add("7");
            comboBoxPacketsize.Items.Add("6");

            comboBoxPacketsize.SelectedItem = "8";

            // Stop bits
            foreach (var item in Enum.GetValues(typeof(StopBits)))
            {
                comboBoxStopbits.Items.Add(item);
            }

            comboBoxStopbits.Items.Remove(StopBits.None); // remove none since this doesn't work
            comboBoxStopbits.SelectedItem = StopBits.One;
        }

        public Create(Main main, SerialSettings serialSettings)
        {
            this.main = main;

            newSerial(serialSettings);
            // TODO use serial settings to open a new session
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            refreshComPorts();
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            // check com
            if (comboBoxCom.SelectedIndex < 0)
            {
                return;
            }

            string com = comboBoxCom.SelectedItem.ToString();

            // check baudrate
            Int32 baudrate = 0;
            if (!Int32.TryParse(comboBoxBaudrate.Text, out baudrate) || baudrate < 1)
            {
                return;
            }

            // check parity
            Parity parity = Parity.None; // TODO ophalen

            // check databits
            Int32 dataBits = 0;
            if (!Int32.TryParse(comboBoxPacketsize.Text, out dataBits) || dataBits < 1)
            {
                return;
            }

            // check stopbits
            StopBits stopBits = StopBits.One;

            // check name
            string name = textBox1.Text;

            newSerial(new SerialSettings(com, baudrate, parity, dataBits, stopBits, name));
        }

        private void newSerial(SerialSettings serialSettings)
        {
            // Create new Serial
            Serial newSerial = new Serial(serialSettings, main);
            main.AddSerial(newSerial);
            newSerial.Show(main.GetDockPanel(), DockState.Document);
            this.Close();
        }

        private void refreshComPorts()
        {
            comboBoxCom.Items.Clear();

            string[] comList = SerialPort.GetPortNames();
            
            for(int i = 0; i < comList.Length; i++)
            {
                comboBoxCom.Items.Add(comList[i]);
            }
        }
    }
}
