﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

using WeifenLuo.WinFormsUI.Docking;

namespace SuperSerial
{
    using System.Collections.Concurrent;
    using System.IO;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;

    enum Timestamp_t
    {
        off,
        time,
        datetime,
        unixtime
    };

    public partial class Serial : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        SerialPort serialPort;
        public SerialSettings serialSettings;

        int maxLines = 10000; // TODO: make this a setting and log old stuff

        BlockingCollection<char> buffer = new BlockingCollection<char>();

        //public delegate void WriteTextDelegate(string text, Color color);
        //public WriteTextDelegate myWriteTextDelegate;

        Timestamp_t timestamps = Timestamp_t.datetime;
        //bool timestamps = true;
        bool isConnected = false;
        bool autoscroll = true;
        bool log = true;
        bool reconnect = true;



        bool nextHasTimeStamp = false;

        enum DisplayMode
        {
            ASCII,
            HEX
        };

        DisplayMode displayMode = DisplayMode.ASCII;
        bool showLineEndings = false;

        Main main;

        // TODO make this a setting
        Color defaultColorReceive = Color.Black;
        Color defaultColorSend = Color.Blue;
        Color defaultColorBackground = Color.White;

        FileStream logStream;
        StreamWriter logWriter;

        public Serial(SerialSettings serialSettings, Main main)
        {
            InitializeComponent();

            this.main = main;

            richTextBox1.Font = Properties.Settings.Default.userFont;

            autoscrollToolStripMenuItem.Checked = autoscroll;
            autoReconnectToolStripMenuItem.Checked = reconnect;
            timerReconnect.Enabled = false;
            
            logToolStripMenuItem.Checked = log;

            this.serialSettings = serialSettings;
            this.Text = serialSettings.com + "_" + serialSettings.name;

            Directory.CreateDirectory("log");

            logStream = File.Open("./log/" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds
                + "_" + Text + ".log", FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            logWriter = new StreamWriter(logStream);

            Connect();
        }

        public void CloseWindow()
        {
            if (serialSettings.connected)
            {
                Disconnect();
            }

            this.Close();
        }

        public void Connect()
        {
            try
            {
                pictureBox1.Image = SuperSerial.Properties.Resources.pausered_fw;

                serialPort = new SerialPort(serialSettings.com, serialSettings.baudrate, serialSettings.parity, serialSettings.dataBits, serialSettings.stopBits);
                serialPort.ReadTimeout = 5;
                serialPort.Handshake = Handshake.None;

                serialPort.Open();
                serialSettings.connected = true;

                WriteText("\n" + serialSettings.com + " opened\n\n", Color.Green);
                //WriteText("\n" + serialSettings.com + " closed\n\n", Color.Green

                serialPort.DataReceived += new SerialDataReceivedEventHandler(SerialReceiveHandler);
                timerWriteText.Enabled = true;

                isConnected = true;
                timerReconnect.Enabled = reconnect;
            }
            catch
            {
                pictureBox1.Image = SuperSerial.Properties.Resources.play_fw;

                serialPort.DataReceived -= new SerialDataReceivedEventHandler(SerialReceiveHandler);
                timerWriteText.Enabled = false;

                serialSettings.connected = false;
                timerWriteText.Enabled = false;

                WriteText("\nPort could not be opened\n\n", Color.Red);

                isConnected = false;
                timerReconnect.Enabled = false;
            }
        }

        public void Disconnect()
        {
            isConnected = false;
            timerReconnect.Enabled = false;

            serialPort.DataReceived -= new SerialDataReceivedEventHandler(SerialReceiveHandler);
            timerWriteText.Enabled = false;

            pictureBox1.Image = SuperSerial.Properties.Resources.play_fw;
            //richTextBox1.AppendText();

            WriteText("\n" + serialSettings.com + " closed\n\n", Color.Green);

            try
            {
                serialPort.Close();
                serialPort.Dispose();
            }
            catch
            {
                // TODO: evt exception meuk hier
                MessageBox.Show("dit mag dus niet meer");
            }
            

            serialSettings.connected = false;
        }

        public void SetFont(Font font)
        {
            this.richTextBox1.Font = font;
        }

        public void ClearWindow()
        {
            richTextBox1.Text = "";
        }


        private void SerialReceiveHandler(object sender, SerialDataReceivedEventArgs e)
        {
            if ((sender as SerialPort).IsOpen)
            {
                int bytesToRead = (sender as SerialPort).BytesToRead;
                var data = new byte[bytesToRead];
                (sender as SerialPort).BaseStream.Read(data, 0, bytesToRead);

                for (int i = 0; i < bytesToRead; i++)
                {
                    buffer.Add(Convert.ToChar(data[i]));
                }
            }
            else
            {
                // close everything, maybe put this all in a try
            }
        }

        //==================================================================================================
        private const int SB_VERT = 0x1;
        private const int WM_VSCROLL = 0x115;
        private const int SB_THUMBPOSITION = 0x4;
        private const int WM_SETREDRAW = 11;
        const int WM_USER = 0x400;
        const int EM_HIDESELECTION = WM_USER + 63;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int GetScrollPos(IntPtr hWnd, int nBar);
        [DllImport("user32.dll")]
        private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);
        [DllImport("user32.dll")]
        private static extern bool PostMessageA(IntPtr hWnd, int nBar, int wParam, int lParam);
        [DllImport("user32.dll")]
        private static extern bool GetScrollRange(IntPtr hWnd, int nBar, out int lpMinPos, out int lpMaxPos);
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, Int32 wParam, Int32 lParam);

        delegate void StringArgReturningVoidDelegate(string text, Color color);

        private void printTimeStamp()
        {
            switch (timestamps)
            {
                default:
                case Timestamp_t.off:
                    break;
                case Timestamp_t.time:
                    string timestamp = DateTime.Now.ToString("HH:mm:ss  ");
                    richTextBox1.AppendText(timestamp, Color.Silver);
                    nextHasTimeStamp = false;
                    break;
                case Timestamp_t.datetime:
                    timestamp = DateTime.Now.ToString("dd-MM-yy HH:mm:ss  ");
                    richTextBox1.AppendText(timestamp, Color.Silver);
                    nextHasTimeStamp = false;
                    break;
                case Timestamp_t.unixtime:
                    timestamp = (DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString("0  ");
                    richTextBox1.AppendText(timestamp, Color.Silver);
                    nextHasTimeStamp = false;
                    break;
            }
        }

        private void WriteText(string text, Color color)
        {
            if (this.richTextBox1.IsDisposed)
            {
                return;
            }

            if (this.richTextBox1.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(WriteText);
                this.richTextBox1.BeginInvoke(d, new object[] { text, color });
            }
            else
            {
                int savedVpos = GetScrollPos(richTextBox1.Handle, SB_VERT);
                bool focused = richTextBox1.Focused;
                
                if (!autoscroll)
                {
                    if (focused) textBox1.Focus();
                    SendMessage(richTextBox1.Handle, EM_HIDESELECTION, 1, 0);
                }

                ////////////////////////////////////////////////////////////////////////////

                if(nextHasTimeStamp)
                {
                    printTimeStamp();
                }

                string[] split = Regex.Split(text, @"(?<=[\n])");  //text.Split('\n');

                foreach(string s in split)
                {
                    string output = s;

                    if(s != "")
                    {
                        if(nextHasTimeStamp)
                        {
                            printTimeStamp();
                        }

                        int lfPos = s.Length - 1;
                        int crPos = s.Length - 2;


                        if(lfPos > -1 && s[lfPos] == '\n')
                        {
                            nextHasTimeStamp = true;

                            if(showLineEndings)
                            {

                                if (crPos > -1 && s[crPos] == '\r')
                                {
                                    richTextBox1.AppendText(output.Substring(0, output.Length - 2), color);
                                    richTextBox1.SelectionBackColor = Color.Blue;
                                    richTextBox1.AppendText("<CR>", Color.White);
                                }
                                else
                                {
                                    richTextBox1.AppendText(output.Substring(0, output.Length - 1), color);
                                }

                                richTextBox1.SelectionBackColor = Color.Blue;
                                richTextBox1.AppendText("<LF>", Color.White);
                                richTextBox1.AppendText(Environment.NewLine);
                            }
                            else
                            {
                                richTextBox1.AppendText(output, color);
                            }
                        }
                        else
                        {
                            richTextBox1.AppendText(output, color);
                        }
                    } 
                }

                if (richTextBox1.Lines.Count() > maxLines + 100)
                {
                    if (log)
                    {
                        logWriter.Write(richTextBox1.Text.Substring(0, richTextBox1.GetFirstCharIndexFromLine(100)));
                        logWriter.Flush();
                    }

                    richTextBox1.ReadOnly = false;
                    richTextBox1.SelectionStart = 0;
                    richTextBox1.SelectionLength = richTextBox1.GetFirstCharIndexFromLine(100); // -1
                    richTextBox1.SelectedText = "";
                    richTextBox1.ReadOnly = true;
                }

                /////////////////////////////////////////////////////////////////////////////

       
                if (autoscroll)
                {
                    int VSmin, VSmax;
                    GetScrollRange(richTextBox1.Handle, SB_VERT, out VSmin, out VSmax);
                    int sbOffset = (int)((richTextBox1.ClientSize.Height - SystemInformation.HorizontalScrollBarHeight) / (richTextBox1.Font.Height));
                    savedVpos = VSmax - sbOffset;
                }
                SetScrollPos(richTextBox1.Handle, SB_VERT, savedVpos, true);
                PostMessageA(richTextBox1.Handle, WM_VSCROLL, SB_THUMBPOSITION + 0x10000 * savedVpos, 0);

                if (!autoscroll)
                {
                    if (focused) textBox1.Focus();
                    SendMessage(richTextBox1.Handle, EM_HIDESELECTION, 1, 0);
                }
            }
        }

        ~Serial()
        {
            isConnected = false;
            timerReconnect.Enabled = false;
            serialPort.DataReceived -= new SerialDataReceivedEventHandler(SerialReceiveHandler);
            

            try
            {
                serialPort.Dispose();
            }
            catch
            {
                // TODO exception
            }
        }

        private void ConvertAsciiToHex()
        {
            string input = richTextBox1.SelectedText;
            string output = "";

            foreach(char c in input)
            {
                // TODO: afhandelen zoopi
                output += Convert.ToByte(c).ToString("X2") + " ";
            }

            richTextBox1.SelectedText = output;
        }

        bool writeTextBusy = false;

        private void timerWriteText_Tick(object sender, EventArgs e)
        {
            if (!writeTextBusy)
            {
                writeTextBusy = true;

                string input = "";

                try
                {
                    char c;
                    while (buffer.TryTake(out c))
                    {
                        input += c;
                    }
                }
                catch
                {
                    // nothing left to take
                }

                if (input.Length > 0)
                {
                    //var parts = input.Split("[INFO]");
                    //input.IndexOf("[INFO]");

                    WriteText(input, defaultColorReceive);
                }
                
                writeTextBusy = false;
            }
        }

        private void Serial_FormClosing(object sender, FormClosingEventArgs e)
        {
            // make sure last stuff is in log
            serialPort.DataReceived -= new SerialDataReceivedEventHandler(SerialReceiveHandler);
            isConnected = false;
            timerReconnect.Enabled = false;


            logWriter.Write(richTextBox1.Text);
            logWriter.Flush();

            

            try
            {
                serialPort.Dispose();
            }
            catch
            {
                // TODO exception
            }

            main.RemoveSerial(this);
        }

        private void cleardelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(serialSettings.connected)
            {
                Disconnect();
            }
            else
            {
                Connect();
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if(serialSettings.connected)
                {
                    // TODO maybe remove \n
                    richTextBox1.AppendText(textBox1.Text + "\n", defaultColorSend);
                    serialPort.Write(textBox1.Text);                   
                }
                else
                {
                    richTextBox1.AppendText("Could not send: " + textBox1.Text + "\n", Color.Red);
                }
            }
        }

        private void Serial_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Delete && e.Modifiers != Keys.Shift && !textBox1.Focused)
            {
                richTextBox1.Text = "";
                e.SuppressKeyPress = true;
            }
        }

        private void autoscrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            autoscroll = this.autoscrollToolStripMenuItem.Checked;
        }

        private void logToolStripMenuItem_Click(object sender, EventArgs e)
        {
            log = this.logToolStripMenuItem.Checked;
        }

        private void autoReconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (autoReconnectToolStripMenuItem.Checked)
            {
                timerReconnect.Enabled = false;
                autoReconnectToolStripMenuItem.Checked = false;
                reconnect = false;
            }
            else
            {
                
                autoReconnectToolStripMenuItem.Checked = true;
                reconnect = true;

                if (isConnected)
                {
                    reconnect = true;
                    timerReconnect.Enabled = true;
                }
            }
        }

        private void convertSelectionToHexToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConvertAsciiToHex();
        }

        private void aSCIIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetDisplayMode(DisplayMode.ASCII);
        }

        private void hexToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetDisplayMode(DisplayMode.HEX);
        }

        private void SetDisplayMode(DisplayMode displayMode)
        {
            this.displayMode = displayMode;
            switch(this.displayMode)
            {
                case DisplayMode.ASCII:
                    hexToolStripMenuItem.Checked = false;
                    aSCIIToolStripMenuItem.Checked = true;
                    break;
                case DisplayMode.HEX:
                    hexToolStripMenuItem.Checked = true;
                    aSCIIToolStripMenuItem.Checked = false;
                    break;
            }
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showLineEndings = !showLineEndings;
            showToolStripMenuItem.Checked = showLineEndings;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Connect();
        }

        private void timerReconnect_Tick(object sender, EventArgs e)
        {
            try
            {
                serialPort.Open();
            }
            catch
            {
                // doen we niks mee
            }
        }

        private void timestampsToolStripMenuItem_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem == TSnoneToolStripMenuItem)
            {
                timestamps = Timestamp_t.off;
                TSnoneToolStripMenuItem.Checked = true;
                TSdateTimeToolStripMenuItem.Checked = false;
                TStimeToolStripMenuItem.Checked = false;
                TSunixToolStripMenuItem.Checked = false;
            }

            if (e.ClickedItem == TStimeToolStripMenuItem)
            {
                timestamps = Timestamp_t.time;
                TSnoneToolStripMenuItem.Checked = false;
                TSdateTimeToolStripMenuItem.Checked = false;
                TStimeToolStripMenuItem.Checked = true;
                TSunixToolStripMenuItem.Checked = false;
            }

            if (e.ClickedItem == TSdateTimeToolStripMenuItem)
            {
                timestamps = Timestamp_t.datetime;
                TSnoneToolStripMenuItem.Checked = false;
                TSdateTimeToolStripMenuItem.Checked = true;
                TStimeToolStripMenuItem.Checked = false;
                TSunixToolStripMenuItem.Checked = false;
            }

            if (e.ClickedItem == TSunixToolStripMenuItem)
            {
                timestamps = Timestamp_t.unixtime;
                TSnoneToolStripMenuItem.Checked = false;
                TSdateTimeToolStripMenuItem.Checked = false;
                TStimeToolStripMenuItem.Checked = false;
                TSunixToolStripMenuItem.Checked = true;
            }
        }
    }

    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
}
